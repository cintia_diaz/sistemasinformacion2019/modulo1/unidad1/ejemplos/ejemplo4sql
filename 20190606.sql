﻿DROP DATABASE IF EXISTS b20190606;
CREATE DATABASE b20190606;
USE b20190606;

-- ejemplo socio, peliculas, alquila con multivaluados

  CREATE OR REPLACE TABLE socio(
    codSocio varchar(10),
    nombre varchar(20),
    PRIMARY KEY (codSocio)
    );

  CREATE OR REPLACE TABLE emails(
    socio varchar(10),
    email varchar(30),
    PRIMARY KEY (socio, email),
    CONSTRAINT fkemailsocio FOREIGN KEY (socio) REFERENCES socio(codSocio)
    );

  CREATE OR REPLACE TABLE telefonoss(
    socio varchar(10),
    telefono varchar(10),
    PRIMARY KEY (socio, telefono),
    CONSTRAINT fktelefonosocio FOREIGN KEY (socio) REFERENCES socio(codSocio)
    );

  CREATE OR REPLACE TABLE peliculas(
    codPel varchar(10),
    titulo varchar(30),
    PRIMARY KEY (codPel)
    );

  CREATE OR REPLACE TABLE alquila(
    socio varchar(10),
    pelicula varchar(10),
    PRIMARY KEY (socio, pelicula),
    CONSTRAINT fkalquilasocio FOREIGN KEY (socio) REFERENCES socio(codSocio),
    CONSTRAINT fkalquilapeli FOREIGN KEY (pelicula) REFERENCES peliculas(codPel)
    );

  CREATE OR REPLACE TABLE alquilaFecha(
    socio varchar(10),
    pelicula varchar(10),
    fecha date,
    PRIMARY KEY (socio, pelicula, fecha),
    CONSTRAINT fkalqfecha_alquila FOREIGN KEY (socio, pelicula) REFERENCES alquila (socio, pelicula)
    );